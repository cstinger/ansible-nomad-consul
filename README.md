# Ansible Nomad Consul practice

a practice of construct Nomad & Consul clusters by Ansible

# Pre-Requirement
3 nodes installed with alpine linux,
and finish following steps in all of the nodes:
- `adduser apuser`
- `apk add sudo python3`
- `addgroup apuser wheel`
- `echo "%wheel ALL=(ALL) NOPASSWD: ALL" >> /etc/sudoers`


and perform follow command on your ansible instances:
- `ansible-galaxy collection install community.general`
