datacenter = "dc1"
data_dir             = "/var/lib/nomad"
disable_update_check = true
enable_syslog        = true

client {
    enabled = true
    network_interface = "eth1"
}

